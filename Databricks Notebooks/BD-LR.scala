// Databricks notebook source
// Loading necessary library
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
// Machine Learning lib
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{HashingTF,IDF, Tokenizer}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.feature.StopWordsRemover

import org.apache.spark.ml.classification.{LogisticRegression, OneVsRest}

import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.classification.LinearSVC

import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator

import org.apache.spark.mllib.evaluation.MulticlassMetrics

import org.apache.spark.ml.classification.{DecisionTreeClassificationModel,DecisionTreeClassifier}
import org.apache.spark.ml.classification.{RandomForestClassificationModel, RandomForestClassifier}
import org.apache.spark.ml.classification.{GBTClassificationModel, GBTClassifier}
import org.apache.spark.ml.classification.{NaiveBayes,NaiveBayesModel}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.feature.StopWordsRemover
import org.apache.spark.ml.classification.{LogisticRegression, OneVsRest}
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.classification.LinearSVC
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.ml.classification.{DecisionTreeClassificationModel, DecisionTreeClassifier}
import org.apache.spark.ml.classification.{RandomForestClassificationModel, RandomForestClassifier}
import org.apache.spark.ml.classification.{GBTClassificationModel, GBTClassifier}
import org.apache.spark.ml.classification.{NaiveBayes, NaiveBayesModel}

// COMMAND ----------

// ======================================== Load data =====================================================
// val data = spark.read.option("header", "true").option("quote", "\"").
// option("escape","\"").option("multiLine",true).option("inferSchema",true).csv("/FileStore/tables/train.csv")
// display(data)

val train_url = "/FileStore/tables/data_train_clean.csv"
val data_train = spark.read.option("header", "true").option("inferSchema",true).csv(train_url)

val test_url = "/FileStore/tables/data_test_clean.csv"
val data_test = spark.read.option("header", "true").option("inferSchema",true).csv(test_url)

// COMMAND ----------

display(data_train)

// COMMAND ----------

// ========================================== PARAMETERS =======================================================
// Change paremeters to build model for different label "toxic","severe_toxic","obscene","threat","insult","identity_hate"
val colName = "identity_hate"

// COMMAND ----------

// ================================= HELPER FUNCTION =================================================

// Description: Add higher weight for label "1"
// Parameter:
// - dataset: DataFrame we need to transform
// - label_name: Name of Label Column
// Return: New DataFrame with "classWeightCol" Column
def balanceDataset(dataset: DataFrame, label_name: String): DataFrame = {

    // Re-balancing (weighting) of records to be used in the logistic loss objective function
    val numPos = dataset.filter(dataset(label_name) === 1).count
    val datasetSize = dataset.count
    val balancingRatio = (datasetSize - numPos).toDouble / datasetSize

    val calculateWeights = udf { d: Double =>
      if (d == 1.0) {
        1 * balancingRatio
      }
      else {
        (1 * (1.0 - balancingRatio))
      }
    }

    val weightedDataset = dataset.withColumn("classWeightCol", calculateWeights(dataset(label_name)))
    weightedDataset
}

// Description: Evaluate your model with various metrics
// Parameter:
// - data: DataFrame we need to evaluate the model on
// - label_name: Name of Label Column
// Return: Map of metrics
// - accuracy, precision by Label, recall by Label, f1 by Label, TPR by Label, FPR by Label ( Label is in order '0', '1')
def evaluation(data: DataFrame,label_name: String): Map[String,Array[Double]] = {
  // Select prediction and label column
  val pred_label = data.select("prediction",label_name).withColumnRenamed(label_name, "label")
  
  // Convert to RDD
  val eval_rdd = pred_label.rdd.map{case Row(prediction:Double,label:Int) =>(prediction,label.toDouble)}
  
  val metric = new MulticlassMetrics(eval_rdd)  
  
  val accuracy = Array(metric.accuracy)
  val confusionMatrix = metric.confusionMatrix
  val fMeasure = Array(metric.fMeasure(0),metric.fMeasure(1))
  val precision = Array(metric.precision(0),metric.precision(1))
  val recall = Array(metric.recall(0),metric.recall(1))
  val TPR = Array(metric.truePositiveRate(0),metric.truePositiveRate(1))
  val FPR = Array(metric.falsePositiveRate(0),metric.falsePositiveRate(1))

  // Return result
  val result = Map("acc" -> accuracy, "precision" -> precision,
                   "recall" -> recall, "f1" -> fMeasure,
                  "TPR" -> TPR, "FPR" -> FPR)
  return result
}

// COMMAND ----------

// ======================================= Feature Extraction ======================================
// (Tokenizer -> StopWord Filter -> TFIDF / Word2Vec / CountVectorizer )
// Reference
// https://www.kaggle.com/danielokeeffe/text-classification-with-apache-spark

val tokenizer = new Tokenizer().setInputCol("clean_comment").setOutputCol("words")
val stopWord = new StopWordsRemover().setInputCol(tokenizer.getOutputCol).setOutputCol("stopWordFilter")

val hashingTF = new HashingTF().setInputCol(stopWord.getOutputCol).setOutputCol("features")
// val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")

// val countVector = new CountVectorizer().setInputCol(stopWord.getOutputCol)
// .setOutputCol("features")
// .setVocabSize(3)
// .setMinDF(2) 

// val word2Vec = new Word2Vec()
//   .setInputCol("stopWordFilter")
//   .setOutputCol("features")
// //   .setVectorSize(30)
//   .setMinCount(0)

val pipeline = new Pipeline().setStages(Array(tokenizer, stopWord,hashingTF)).fit(data_train)
// val pipeline = new Pipeline().setStages(Array(tokenizer, stopWord,word2Vec))

var data_train_pro = pipeline.transform(data_train).select("id","features","toxic","severe_toxic","obscene","threat","insult","identity_hate")
data_train_pro = balanceDataset(data_train_pro,colName)
val data_test_pro = pipeline.transform(data_test).select("id","features","toxic","severe_toxic","obscene","threat","insult","identity_hate")

// COMMAND ----------

// Change the model and parameters here
val metricName = "f1"
val numFold = 5
val lr = new LogisticRegression()
  .setMaxIter(100)
  .setWeightCol("classWeightCol")
//   .setRegParam(0.3)
//   .setElasticNetParam(0.8)
//   .setFamily("multinomial")
  .setLabelCol(colName)

val paramGrid = new ParamGridBuilder()
  .addGrid(lr.regParam, Array(0.0001, 0.00015, 0.0002, 0.00025 )).addGrid(lr.elasticNetParam, Array(0, 0.5, 1))
  .build()

val cv = new CrossValidator()
  .setEstimator(lr)
  .setEvaluator(new MulticlassClassificationEvaluator().setLabelCol(colName).setMetricName(metricName))
  .setEstimatorParamMaps(paramGrid)
  .setNumFolds(numFold)  // Use 3 to test

// API: https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.ml.classification.LogisticRegression

// COMMAND ----------

// ========================================== Fit the best CV model ================================================
val cvModel = cv.fit(data_train_pro)
// API: https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.ml.classification.LogisticRegression

// COMMAND ----------

// ========================================== Get the best Parameter for model (Don't need to run this if you use Naive Bayes) ================================================
// Get the best parameter according to defined metrics
val bestParam = cvModel.avgMetrics.zipWithIndex.sortBy(-_._1).take(1).map(_._2)
cvModel.getEstimatorParamMaps(bestParam(0))

// COMMAND ----------

// ======================================== EVALUATION on TRAIN / TEST =========================================================
// On training data
// val eval_data_train = cvModel.transform(data_train_pro)
// evaluation(eval_data_train,colName)
// On Test data
val eval_data_test = cvModel.transform(data_test_pro)
evaluation(eval_data_test,colName)
